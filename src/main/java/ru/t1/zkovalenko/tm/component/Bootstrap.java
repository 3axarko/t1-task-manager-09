package ru.t1.zkovalenko.tm.component;

import ru.t1.zkovalenko.tm.api.ICommandController;
import ru.t1.zkovalenko.tm.api.ICommandRepository;
import ru.t1.zkovalenko.tm.api.ICommandService;
import ru.t1.zkovalenko.tm.constant.ArgumentsConst;
import ru.t1.zkovalenko.tm.constant.TerminalConst;
import ru.t1.zkovalenko.tm.controller.CommandController;
import ru.t1.zkovalenko.tm.repository.CommandRepository;
import ru.t1.zkovalenko.tm.service.CommandService;

import java.util.Locale;
import java.util.Scanner;

public class Bootstrap {

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final ICommandController commandController = new CommandController(commandService);

    private void processArgument(final String arg) {
        switch (arg.toLowerCase(Locale.ENGLISH)) {
            case TerminalConst.VERSION:
            case ArgumentsConst.VERSION:
                commandController.showVersion();
                break;
            case TerminalConst.HELP:
            case ArgumentsConst.HELP:
                commandController.showHelp();
                break;
            case TerminalConst.ABOUT:
            case ArgumentsConst.ABOUT:
                commandController.showAbout();
                break;
            case TerminalConst.INFO:
            case ArgumentsConst.INFO:
                commandController.showInfo();
                break;
            case TerminalConst.ARGUMENTS:
            case ArgumentsConst.ARGUMENTS:
                commandController.showArguments();
                break;
            case TerminalConst.COMMANDS:
            case ArgumentsConst.COMMANDS:
                commandController.showCommands();
                break;
            case TerminalConst.EXIT:
                exit();
            default:
                commandController.showError();
        }
    }

    private void exit() {
        System.exit(0);
    }

    public void run (String[] args) {
        if (processArguments(args)) exit();

        commandController.showWelcome();

        final Scanner scanner = new Scanner(System.in);
        while (true) {
            System.out.println("Enter Command:");
            final String command = scanner.nextLine();
            System.out.println("---");
            processArgument(command);
        }
    }

    private boolean processArguments(final String[] args) {
        if (args == null || args.length == 0) return false;
        final String arg = args[0];
        processArgument(arg);
        return true;
    }

}
