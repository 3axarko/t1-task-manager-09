package ru.t1.zkovalenko.tm.service;

import ru.t1.zkovalenko.tm.api.ICommandRepository;
import ru.t1.zkovalenko.tm.api.ICommandService;
import ru.t1.zkovalenko.tm.model.Command;

public class CommandService implements ICommandService {

    private final ICommandRepository commandRepository;

    public CommandService(final ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    @Override
    public Command[] getTerminalControls() {
        return commandRepository.getTerminalControls();
    }

}
