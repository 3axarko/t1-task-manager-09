package ru.t1.zkovalenko.tm.repository;

import ru.t1.zkovalenko.tm.api.ICommandRepository;
import ru.t1.zkovalenko.tm.constant.ArgumentsConst;
import ru.t1.zkovalenko.tm.constant.TerminalConst;
import ru.t1.zkovalenko.tm.model.Command;

public class CommandRepository implements ICommandRepository {

    public static Command VERSION = new Command(
            TerminalConst.VERSION, ArgumentsConst.VERSION,
            "Show app version"
    );

    public static Command ABOUT = new Command(
            TerminalConst.ABOUT, ArgumentsConst.ABOUT,
            "Who did it? And why?"
    );

    public static Command HELP = new Command(
            TerminalConst.HELP, ArgumentsConst.HELP,
            "App commands"
    );

    public static Command EXIT = new Command(
            TerminalConst.EXIT, null,
            "Quit from program"
    );

    public static Command INFO = new Command(
            TerminalConst.INFO, ArgumentsConst.INFO,
            "Info about system"
    );

    public static Command ARGUMENTS = new Command(
            TerminalConst.ARGUMENTS, ArgumentsConst.ARGUMENTS,
            "List of arguments"
    );

    public static Command COMMANDS = new Command(
            TerminalConst.COMMANDS, ArgumentsConst.COMMANDS,
            "List of Available commands"
    );

    private static Command[] TERMINAL_CONTROLS = new Command[]{
            INFO, ABOUT, VERSION, HELP, COMMANDS, ARGUMENTS, EXIT
    };

    @Override
    public Command[] getTerminalControls(){
        return TERMINAL_CONTROLS;
    }

}
